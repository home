# $Id:dot.kshrc,v 1.4 2016/09/28 19:12:25 fstef Exp $

# call the system config file
. /etc/ksh.kshrc



# set the prompt
#export PS1="\e[0;34m\!\e[m [\e[0;33m\w\e[m] \e[0;36m\j\e[m\n\e[0;32m\$\e[m "
case "$TERM" in
    *-256color)
        PS1="[\e[0;37m\w\e[m] \e[0;31m\j\e[m\n\e[0;37m\$\e[m "
        ;;
    *)
        PS1="[\w] \j\n\$ "
        ;;
esac
export PS1

# set the pager
PAGER=less
MANPAGER=less
export PAGER MANPAGER

# set the command line editor to be like vi
VISUAL=vi
export VISUAL

# set the git editor
GIT_EDITOR=/usr/local/bin/nvi
export GIT_EDITOR

# set history file
HISTFILE=$HOME/.sh-hist
export HISTFILE

# define some useful aliases
alias ..='cd ..'
alias cd..='cd ..'
alias cd...='cd ../..'
alias cd....='cd ../../..'
alias cd.....='cd ../../../..'
alias cd/='cd /'
alias l='ls -A'
alias la='ls -nAh'
alias ll='ls -lh'
alias when='when --noampm'

# don't clobber existing files
set -o noclobber

MM_CHARSET=en_US.UTF-8
export MM_CHARSET

