#!/bin/sh
# $Id: bin/tmuxbar.sh,v 1.1 2016/11/15 12:27:48 fste Exp $ 
#
# dzen configuration file
#

# ------------------------------------------------------------------------------
# Copywhat (?) 2016 stefano ferro <fstef@cryptolab.net>
#
# This work is protected by the Berne Convention but nobody can tell us
# what we can do and what we can't, so we don't care.
# Anybody caught use, copy, hacking, and distribute this work for any 
# purpose, or otherwise experience it, without our permission. 
# Credit the creator of this work is a matter of ethics, not a matter 
# of law, so is up to you.
#
# THE WORK IS PROVIDED "AS IS" AND YOU USE IT AT YOUR OWN RISK.
# ------------------------------------------------------------------------------

print_delim() {
	echo -n " #[fg=#008b8b]::#[default] "
}

print_date() {
	FORMAT="%a, %b %d %R"
	DATE=$(date "+${FORMAT}")
	echo -n "#[fg=#ffdead]${DATE}#[default]"
}

print_volume() {
	VOLSTAT=$(mixerctl outputs.master.mute | sed -e 's/outputs.master.mute=//')
	if [ $VOLSTAT == "off" ]; then
		echo -n "#[fg=#ff7f50]=#[default]"
	else
		echo -n " "
	fi
}

print_mail() {
	NMAIL=$(new | awk '/total/ {print $2}' | sed -e 's/\.//')
	if [[ $NMAIL -eq 0 ]]; then
		echo -n "@"
	else
		echo -n "#[fg=#ff7f50]@#[default]${NMAIL}"
	fi
}

print_temp() {
    TMP=$(sysctl | grep acpitz | sed -e 's/hw.sensors.acpitz0.temp0=//' -e 's/.00 degC (zone temperature)/°/')
	if [ $TMP -lt 75 ]; then
		echo -n "t:#[fg=#800020]${TMP}#[default]"
	else
		echo -n "t:${TMP}"
	fi
}

print_fan() {
    FAN=$(sysctl | grep fan | sed -e 's/hw.sensors.acpithinkpad0.fan0=//' -e 's/ //')
    echo -n "f:${FAN}"
}

print_mem() {
	MEM=$(/usr/bin/top | grep Free: | cut -d " " -f6)
	echo -n "m:${MEM}"
}

print_apm() {
	BAT_STATUS=$1
	BAT_LEVEL=$2
	AC_STATUS=$3
	if [ $AC_STATUS -ne 255 -o $BAT_STATUS -lt 4 ]; then
		if [ $AC_STATUS -eq 0 ]; then

			if [ $BAT_STATUS -eq 0 ]; then
				echo -n "#[fg=#00ff00][#[default]${BAT_LEVEL}%#[fg=#00ff00]]#[default]"
			fi
			if [ $BAT_STATUS -eq 1 ]; then
				echo -n "#[fg=#ffff00][#[default]${BAT_LEVEL}%#[fg=#ffff00]]#[default]"
			fi
			if [ $BAT_STATUS -eq 2 ]; then
				echo -n "#[fg=#ff0000][#[default${BAT_LEVEL}%#[fg=#ff0000]]#[default]"
			fi
			if [ $BAT_STATUS -eq 3 ]; then
				echo -n "battery charging #[fg=#00ff00]${BAT_LEVEL}%#[default]"
			fi
		else
			case $AC_STATUS in
			1)
				AC_STRING="#[fg=#ff7f50]+#[default]"
				;;
			2)
				AC_STRING="backup +"
				;;
			*)
				AC_STRING=""
				;;
			esac;
			case $BAT_STATUS in
			4)
					BAT_STRING="no battery"
					;;
			[0-3])
					BAT_STRING="${BAT_LEVEL}%"
					;;
			*)
					BAT_STRING="battery unknown"
					;;
			esac;

			FULL="${AC_STRING}${BAT_STRING}"
			if [ "$FULL" != "" ]; then
				echo -n "[$FULL]"
			fi
		fi
	fi
}

print_cpuspeed() {
	CPU_SPEED=$(/sbin/sysctl hw.cpuspeed | cut -d "=" -f2)
	echo -n " cpu:${CPU_SPEED}MHz "
}

			
# instead of sleeping, use iostat as the update timer.
# cache the output of apm(8), no need to call that every second.
/usr/sbin/iostat -C -c 1 |&	# wish infinity was an option
PID="$!"
APM_DATA=""
I=0
trap "kill $PID; exit" TERM
while read -p; do
	if [ $(( ${I} % 1 )) -eq 0 ]; then
		APM_DATA=`/usr/sbin/apm -alb`
	fi
	if [ $I -ge 2 ]; then
#		print_mem
#		print_cpuspeed
		print_delim
        print_temp
		print_delim
#        print_fan
		print_mail
		print_delim
		print_apm $APM_DATA
		print_delim
		print_volume
		print_delim
        print_date
		echo ""

	fi
	I=$(( ( ${I} + 1 ) % 22 ));
done

